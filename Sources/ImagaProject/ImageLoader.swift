import Foundation

struct ImageLoader{
    static func loadImage(from path: String) -> Data?{
        let fileHandle = FileHandle(forReadingAtPath: path)

        guard let validFileHandle = fileHandle else {
            return nil
        }

        return validFileHandle.readDataToEndOfFile()
    }
}
import Foundation

let sleepPeriod : UInt32 = 10

/*ImagaService.shared.getAvailableCategorizersAsync{
    (success) in 
    if success{
        print("GET request succeeded!")
    }else{
        print("GET request failed...")
        
    }
}*/


var data = ImageLoader.loadImage(from:"/home/apollo/Desktop/imagaproj/cat.jpg")
guard let imageData = data else {
    print("Unable to load the specified image.....")
    exit(0)
}

ImagaService.shared.uploadImage(imageData: imageData){
    (success) in
        if success{
        print("Image upload succeeded!")
    }else{
        print("Image upload failed...")
        
    }
}

// ImagaService.shared.getTags(for:"i073ab567f21ca0b2962ec3e13YwGnYi"){
//     (success) in
//     if success{
//         print("Request succeeded!")
//     }else{
//         print("Request failed...")
//     }
// }

/*Wait for the async tasks to finish*/
print("Sleeping for \(sleepPeriod) seconds")
sleep(sleepPeriod)
import Foundation
import FoundationNetworking

class ImagaService{
    private static let API_KEY: String = ""
    private static let API_SECRET: String = ""

    
    /*Singleton*/
    static let shared = ImagaService()

    func getAvailableCategorizersAsync(completion: @escaping (Bool) -> Void){
        let categorizersComponents  = createURLComponents(path: "/v2/categorizers")

        guard let composedURL = categorizersComponents.url else {
            print("Component composition failed")
            return
        }

        var request = URLRequest(url: composedURL)
        request.httpMethod = "GET"
        guard let authStringBase64 = base64EncodedAuthenticationString() else {
            print("Error generating authentication string....")
            completion(false)
            return
        }
        request.setValue("Basic \(authStringBase64)", forHTTPHeaderField:"Authorization")
        URLSession.shared.dataTask(with:request){
            (data, response, error) in 
            if let httpResponse = response as? HTTPURLResponse{
                print("Status code: \(httpResponse.statusCode)")
            }
            
            guard let validData = data, error == nil else{
                completion(false)
                return
            }
            do{
                let json = try JSONSerialization.jsonObject(with: validData, options:[])
                print(json)
                completion(true)
            }catch let serializationError{
                print(serializationError)
                completion(false)
            }
        }.resume()
    }

    func uploadImage(imageData: Data,completion: @escaping (Bool) -> Void){
        let uploadComponents  = createURLComponents(path: "/v2/uploads")

        guard let composedURL = uploadComponents.url else {
            print("Component composition failed")
            completion(false)
            return
        }

        let boundary = "Boundary-\(UUID().uuidString)"
        var request = URLRequest(url: composedURL)
        request.httpMethod = "POST"

        guard let authStringBase64 = base64EncodedAuthenticationString() else {
            print("Error generating authentication string....")
            completion(false)
            return
        }

        request.setValue("Basic \(authStringBase64)", forHTTPHeaderField:"Authorization")
        request.setValue("Keep-Alive", forHTTPHeaderField:"Connection")
        request.setValue("no-cache", forHTTPHeaderField:"Cache-Control")
        request.setValue("multipart/form-data;boundary=\(boundary)", forHTTPHeaderField:"Content-Type")

        let httpBody = NSMutableData()

        httpBody.append(convertFileData(fieldName:"image", fileName: "cat.jpg", mimeType:"image/jpeg", fileData:imageData, using:boundary))
        httpBody.appendString("--\(boundary)--")
        request.httpBody = httpBody as Data

        URLSession.shared.dataTask(with: request) { 
            (data, response, error) in
            
            if let httpResponse = response as? HTTPURLResponse{
                print("API status: \(httpResponse.statusCode)")
            }

            guard let validData = data, error == nil else{
                print(error!)
                completion(false)
                return
            }

            do{
                let json = try JSONSerialization.jsonObject(with: validData, options:[])
                print(json)
                completion(true)
            }catch let serializationError{
                print(serializationError)
                completion(false)
            }

        }.resume()

        
    }

    func createURLComponents(path:String) -> URLComponents{
        var components = URLComponents()
        components.scheme = "https"
        components.host = "api.imagga.com"
        components.path = path
        return components
    }

    func getTags(for imageID: String, completion: @escaping (Bool) -> Void){
        let tagURL  = "https://api.imagga.com/v2/tags?image_upload_id=\(imageID)"
        let url = URL(string: tagURL)
        guard let composedURL = url else {
            print("Component composition failed")
            completion(false)
            return
        }

        var request = URLRequest(url: composedURL)
        
        request.httpMethod = "GET"
        
        guard let authStringBase64 = base64EncodedAuthenticationString() else {
            print("Error generating authentication string....")
            completion(false)
            return
        }

        request.setValue("Basic \(authStringBase64)", forHTTPHeaderField:"Authorization")

        URLSession.shared.dataTask(with:request){
            (data, response, error) in 
            if let httpResponse = response as? HTTPURLResponse{
                print("Status code: \(httpResponse.statusCode)")
            }
            
            guard let validData = data, error == nil else{
                completion(false)
                return
            }
            do{
                let json = try JSONSerialization.jsonObject(with: validData, options:[])
                print(json)
                completion(true)
            }catch let serializationError{
                print(serializationError)
                completion(false)
            }
        }.resume()

    }

    func convertFileData(fieldName: String, fileName: String, mimeType: String, fileData: Data, using boundary: String) -> Data {
        let data = NSMutableData()

        data.appendString("--\(boundary)\r\n")
        data.appendString("Content-Disposition: form-data; name=\"\(fieldName)\"; filename=\"\(fileName)\"\r\n")
        data.appendString("\r\n")
        data.append(fileData)
        data.appendString("\r\n")
        return data as Data
    }

    func base64EncodedAuthenticationString() -> String? {
        let authString = ImagaService.API_KEY + ":" + ImagaService.API_SECRET
        var authStringBase64 = ""
        if let authData = authString.data(using: .utf8){
            authStringBase64 = authData.base64EncodedString()
            return authStringBase64
        }
        return nil
    }

}

extension NSMutableData {
    func appendString(_ string: String) {
        if let data = string.data(using: .utf8) {
            self.append(data)
        }
    }
}
